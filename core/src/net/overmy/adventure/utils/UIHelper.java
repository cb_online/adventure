package net.overmy.adventure.utils;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad.TouchpadStyle;

import net.overmy.adventure.Core;
import net.overmy.adventure.resources.FontAsset;
import net.overmy.adventure.resources.IMG;

/*
     Created by Andrey Mikheev on 29.09.2017
     Contact me → http://vk.com/id17317
 */

public final class UIHelper {

    private UIHelper() {
    }



    public static Label Label( String text, FontAsset currentFont ) {
        Label returnLabel = new Label( text, currentFont.getStyle() );

        float x = -returnLabel.getWidth() / 2;
        float y = -returnLabel.getHeight() / 2;
        returnLabel.setPosition( x, y );

        return returnLabel;
    }
/*
    public static Label LabelWithWrap( final Text text, final FontAsset currentFont ) {
        return LabelWithWrap( text, currentFont );
    }*/



    public static Label LabelWithWrap( final String text, final FontAsset currentFont ) {
        Label returnLabel = new Label( text, currentFont.getStyle() );

        returnLabel.setWidth( Core.WIDTH * 0.7f );
        returnLabel.setWrap( true );

        float x = -returnLabel.getWidth() / 2;
        float y = -returnLabel.getHeight() / 2;
        returnLabel.setPosition( x, y );

        return returnLabel;
    }



    /**
     * Создаем ГРУППУ и добавляем к ней АКТЁРА
     */
    public static Group convertActorToGroup( final Actor actor ) {
        final Group returnGroup = new Group();
        returnGroup.addActor( actor );
        return returnGroup;
    }



    /**
     * Группа появляется из 0 в 1 за время (FADE * 2)
     */
    public static void scaleIn( Actor actor ) {
        final float time = MathUtils.random( Core.FADE * 0.2f, Core.FADE );
        actor.addAction( Actions.sequence( Actions.scaleTo( 0, 0, 0 ),
                                           Actions.scaleTo( 1, 1, time ) ) );
    }



    public static void fall( Actor actor ) {
        final float time = MathUtils.random( Core.FADE * 0.2f, Core.FADE );
        actor.addAction( Actions.sequence( Actions.scaleTo( 2.0f, 2.0f, 0 ),
                                           Actions.scaleTo( 1.0f, 1.0f, time ) ) );
        actor.addAction(
                Actions.parallel(
                        Actions.sequence( Actions.alpha( 0, 0 ),
                                          Actions.alpha( 1, time / 2 ) ),
                        Actions.sequence( Actions.scaleTo( 2.0f, 2.0f, 0 ),
                                          Actions.scaleTo( 1.0f, 1.0f, time ) )
                                )
                       );
    }



    public static void rollIn( Actor actor, float x, float y, float x2, float y2 ) {
        final float time = MathUtils.random( Core.FADE * 0.8f, Core.FADE );
        actor.addAction(
                Actions.parallel(
                        Actions.sequence( Actions.alpha( 0, 0 ),
                                          Actions.alpha( 1, time ) ),
                        Actions.sequence( Actions.moveTo( x, y, 0 ),
                                          Actions.moveTo( x2, y2, time ) )
                                )
                       );
    }



    public static void rollInAndRun( Actor actor, float x, float y, float x2, float y2,
                                     Runnable someRun ) {
        final float time = MathUtils.random( Core.FADE * 0.8f, Core.FADE );
        actor.addAction(
                Actions.parallel(
                        Actions.sequence( Actions.alpha( 0, 0 ),
                                          Actions.alpha( 1, time ) ),
                        Actions.sequence( Actions.moveTo( x, y, 0 ),
                                          Actions.moveTo( x2, y2, time ),
                                          Actions.delay( Core.FADE * 0.2f ),
                                          Actions.run( someRun ) )
                                )
                       );
    }



    public static void rollOut( Actor actor, float x, float y, float x2, float y2 ) {
        final float time = MathUtils.random( Core.FADE * 0.8f, Core.FADE );
        actor.addAction(
                Actions.parallel(
                        Actions.sequence( Actions.alpha( 1, 0 ),
                                          Actions.alpha( 0, time ) ),
                        Actions.sequence( Actions.moveTo( x, y, 0 ),
                                          Actions.moveTo( x2, y2, time ) )
                                )
                       );
    }



    public static void rollOutAndRun( Actor actor, float x, float y, float x2, float y2, Runnable
            runnable ) {
        final float time = MathUtils.random( Core.FADE * 0.8f, Core.FADE );
        actor.addAction(
                Actions.parallel(
                        Actions.sequence( Actions.alpha( 1, 0 ),
                                          Actions.alpha( 0, time ) ),
                        Actions.sequence( Actions.moveTo( x, y, 0 ),
                                          Actions.moveTo( x2, y2, time ),
                                          Actions.run( runnable )
                                        )
                                )
                       );
    }



    /**
     * Группа исчезает из 1 в 0 за время (FADE * 2)
     */
    public static void scaleOut( Actor actor ) {
        final float time = MathUtils.random( Core.FADE * 0.2f, Core.FADE );
        actor.addAction( Actions.sequence( Actions.scaleTo( 1, 1, 0 ),
                                           Actions.scaleTo( 0, 0, time ) ) );
    }



    public static void scaleOutAndRun( final Actor actor, Runnable runnable ) {
        final float time = MathUtils.random( Core.FADE * 0.2f, Core.FADE );

        actor.addAction( Actions.sequence( Actions.scaleTo( 1, 1, 0 ),
                                           Actions.scaleTo( 0, 0, time ),
                                           Actions.run( runnable ) ) );
    }



    public static void waitAndRun( final Actor actor, Runnable runnable ) {
        actor.addAction( Actions.sequence( Actions.delay( Core.FADE ), Actions.run( runnable ) ) );
    }



    public static void waitAndRun( final Actor actor, float time, Runnable runnable ) {
        actor.addAction( Actions.sequence( Actions.delay( time ), Actions.run( runnable ) ) );
    }



    public static Touchpad createTouchPad() {
        final TouchpadStyle touchpadStyle = new TouchpadStyle();

        touchpadStyle.knob = IMG.BUTTON.getDrawable();
        touchpadStyle.knob.setMinWidth( Core.HEIGHT * 0.08f );
        touchpadStyle.knob.setMinHeight( Core.HEIGHT * 0.08f );

        touchpadStyle.background = IMG.PAD.getDrawable();
        touchpadStyle.background.setMinWidth( Core.HEIGHT * 0.4f );
        touchpadStyle.background.setMinHeight( Core.HEIGHT * 0.4f );

        return new Touchpad( Core.HEIGHT * 0.01f, touchpadStyle );
    }



    public static void clickAnimation( Actor actor ) {
        final float timeIn = Core.FADE_HALF * 0.2f;
        final float timeOut = Core.FADE_HALF * 0.8f;
        actor.addAction( Actions.sequence(
                Actions.scaleTo( 0.5f, 0.5f, timeIn, Interpolation.circleIn ),
                Actions.scaleTo( 1.0f, 1.0f, timeOut, Interpolation.circleOut )
                                         ) );
    }
}
