package net.overmy.adventure.ashley.components;

/*
     Created by Andrey Mikheev on 30.09.2017
     Contact me → http://vk.com/id17317
 */

public enum COMP_TYPE {
    COLLECTABLE,
    PICKABLE,
    LADDER,
    GROUND,
    MYPLAYER,
    NPC,
}
