package net.overmy.adventure.ashley;

import com.badlogic.ashley.core.ComponentMapper;

import net.overmy.adventure.ashley.components.AnimationComponent;
import net.overmy.adventure.ashley.components.BVHPhysicalComponent;
import net.overmy.adventure.ashley.components.BoundingComponent;
import net.overmy.adventure.ashley.components.CollectableComponent;
import net.overmy.adventure.ashley.components.DecalComponent;
import net.overmy.adventure.ashley.components.GroundedComponent;
import net.overmy.adventure.ashley.components.InteractComponent;
import net.overmy.adventure.ashley.components.LevelObjectComponent;
import net.overmy.adventure.ashley.components.ModelComponent;
import net.overmy.adventure.ashley.components.MyAnimationComponent;
import net.overmy.adventure.ashley.components.MyPlayerComponent;
import net.overmy.adventure.ashley.components.NPCComponent;
import net.overmy.adventure.ashley.components.OutOfCameraComponent;
import net.overmy.adventure.ashley.components.PhysicalComponent;
import net.overmy.adventure.ashley.components.PositionComponent;
import net.overmy.adventure.ashley.components.RemoveByTimeComponent;
import net.overmy.adventure.ashley.components.RemoveByZoneComponent;
import net.overmy.adventure.ashley.components.TextDecalComponent;
import net.overmy.adventure.ashley.components.TypeOfComponent;

/*
      Created by Andrey Mikheev on 30.09.2017
      Contact me → http://vk.com/id17317
 */

public final class MyMapper {

    public static ComponentMapper< PositionComponent >     POSITION       = null;
    public static ComponentMapper< AnimationComponent >    ANIMATION      = null;
    public static ComponentMapper< MyAnimationComponent >  MY_ANIMATION   = null;
    public static ComponentMapper< PhysicalComponent >     PHYSICAL       = null;
    public static ComponentMapper< BVHPhysicalComponent > BVH_PHYSICAL  = null;
    public static ComponentMapper< ModelComponent >       MODEL         = null;
    public static ComponentMapper< GroundedComponent >    GROUNDED      = null;
    public static ComponentMapper< OutOfCameraComponent > OUT_OF_CAMERA = null;
    public static ComponentMapper< BoundingComponent >    BOUNDS        = null;
    public static ComponentMapper< TypeOfComponent >       TYPE           = null;
    public static ComponentMapper< RemoveByZoneComponent > REMOVE_BY_ZONE = null;
    public static ComponentMapper< RemoveByTimeComponent > REMOVE_BY_TIME = null;
    public static ComponentMapper< MyPlayerComponent >    MY_PLAYER   = null;
    public static ComponentMapper< DecalComponent >       DECAL       = null;
    public static ComponentMapper< InteractComponent >    INTERACT    = null;
    public static ComponentMapper< CollectableComponent > COLLECTABLE = null;
    public static ComponentMapper< NPCComponent >         NPC         = null;
    public static ComponentMapper< TextDecalComponent >   TEXT_DECAL  = null;
    public static ComponentMapper< LevelObjectComponent > LEVEL_OBJECT  = null;



    private MyMapper() {
    }



    public static void init() {
        POSITION = ComponentMapper.getFor( PositionComponent.class );
        ANIMATION = ComponentMapper.getFor( AnimationComponent.class );
        MY_ANIMATION = ComponentMapper.getFor( MyAnimationComponent.class );
        PHYSICAL = ComponentMapper.getFor( PhysicalComponent.class );
        BVH_PHYSICAL = ComponentMapper.getFor( BVHPhysicalComponent.class );
        MODEL = ComponentMapper.getFor( ModelComponent.class );
        GROUNDED = ComponentMapper.getFor( GroundedComponent.class );
        OUT_OF_CAMERA = ComponentMapper.getFor( OutOfCameraComponent.class );
        BOUNDS = ComponentMapper.getFor( BoundingComponent.class );
        TYPE = ComponentMapper.getFor( TypeOfComponent.class );
        REMOVE_BY_ZONE = ComponentMapper.getFor( RemoveByZoneComponent.class );
        REMOVE_BY_TIME = ComponentMapper.getFor( RemoveByTimeComponent.class );
        MY_PLAYER = ComponentMapper.getFor( MyPlayerComponent.class );
        DECAL = ComponentMapper.getFor( DecalComponent.class );
        INTERACT = ComponentMapper.getFor( InteractComponent.class );
        COLLECTABLE = ComponentMapper.getFor( CollectableComponent.class );
        NPC = ComponentMapper.getFor( NPCComponent.class );
        TEXT_DECAL = ComponentMapper.getFor( TextDecalComponent.class );
        LEVEL_OBJECT = ComponentMapper.getFor( LevelObjectComponent.class );
    }



    public static void dispose() {
        POSITION = null;
        ANIMATION = null;
        MY_ANIMATION = null;
        PHYSICAL = null;
        BVH_PHYSICAL = null;
        MODEL = null;
        GROUNDED = null;
        OUT_OF_CAMERA = null;
        BOUNDS = null;
        TYPE = null;
        REMOVE_BY_ZONE = null;
        REMOVE_BY_TIME = null;
        MY_PLAYER = null;
        DECAL = null;
        INTERACT = null;
        COLLECTABLE = null;
        NPC = null;
        TEXT_DECAL = null;
        LEVEL_OBJECT = null;
    }
}
