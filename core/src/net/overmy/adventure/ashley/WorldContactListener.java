package net.overmy.adventure.ashley;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.ContactListener;

import net.overmy.adventure.AshleyWorld;
import net.overmy.adventure.DEBUG;
import net.overmy.adventure.MyPlayer;
import net.overmy.adventure.ashley.components.COMP_TYPE;
import net.overmy.adventure.ashley.components.OutOfCameraComponent;
import net.overmy.adventure.ashley.components.PhysicalComponent;
import net.overmy.adventure.ashley.components.PositionComponent;
import net.overmy.adventure.ashley.components.RemoveByTimeComponent;
import net.overmy.adventure.ashley.components.RemoveByZoneComponent;
import net.overmy.adventure.logic.DynamicLevels;

/*
      Created by Andrey Mikheev on 30.09.2017
      Contact me → http://vk.com/id17317
 */

public class WorldContactListener extends ContactListener {

    private ImmutableArray< Entity > entities;

    private Entity entity1 = null;
    private Entity entity2 = null;

    private boolean       DEBUG_CONTACTS = false;
    private StringBuilder stringBuilder  = new StringBuilder();


    public WorldContactListener () {
        super();
        DEBUG_CONTACTS = DEBUG.CONTACTS.get();
    }


    @Override
    public void onContactProcessed ( int userValue0, boolean match0, int userValue1,
                                     boolean match1 ) {

        if ( DEBUG_CONTACTS ) {
            stringBuilder.setLength( 0 );
            stringBuilder.append( userValue0 );
            stringBuilder.append( match0 ? " (match) " : " (not match)" );
            stringBuilder.append( " -> " );
            stringBuilder.append( userValue1 );
            stringBuilder.append( match1 ? " (match) " : " (not match)" );
            Gdx.app.debug( "onContactProcessed", stringBuilder.toString() );
        }

        for ( Entity entity : entities ) {
            final PhysicalComponent comp = MyMapper.PHYSICAL.get( entity );

            if ( comp.body.getUserValue() == userValue0 ) {
                entity1 = entity;
            }
            if ( comp.body.getUserValue() == userValue1 ) {
                entity2 = entity;
            }
        }

        if ( entity1 == null || entity2 == null ) {
            return;
        }

        if ( !MyMapper.PHYSICAL.has( entity1 ) ||
             !MyMapper.PHYSICAL.has( entity2 ) ) {
            return;
        }

        startContactEntities( entity1, entity2 );
    }


    @Override
    public void onContactEnded ( int userValue0, boolean match0, int userValue1, boolean match1 ) {

        if ( DEBUG_CONTACTS ) {
            stringBuilder.setLength( 0 );
            stringBuilder.append( userValue0 );
            stringBuilder.append( match0 ? " (match) " : " (not match)" );
            stringBuilder.append( " -> " );
            stringBuilder.append( userValue1 );
            stringBuilder.append( match1 ? " (match) " : " (not match)" );
            Gdx.app.debug( "onContactEnded", stringBuilder.toString() );
        }

        for ( Entity entity : entities ) {
            final PhysicalComponent comp = MyMapper.PHYSICAL.get( entity );

            if ( comp.body.getUserValue() == userValue0 ) {
                entity1 = entity;
            }
            if ( comp.body.getUserValue() == userValue1 ) {
                entity2 = entity;
            }
        }

        if ( entity1 == null || entity2 == null ) {
            return;
        }

        if ( !MyMapper.PHYSICAL.has( entity1 ) ||
             !MyMapper.PHYSICAL.has( entity2 ) ) {
            return;
        }

        endContactEntities( entity1, entity2 );
    }


    private void startContactEntities ( Entity entity01, Entity entity02 ) {

        final COMP_TYPE type1 = MyMapper.TYPE.get( entity01 ).type;
        final COMP_TYPE type2 = MyMapper.TYPE.get( entity02 ).type;

        if ( DEBUG_CONTACTS ) {
            stringBuilder.setLength( 0 );
            stringBuilder.append( type1 );
            stringBuilder.append( " -> " );
            stringBuilder.append( type2 );
            Gdx.app.debug( "startContact", stringBuilder.toString() );
        }

        final boolean contact2Ground = type2.equals( COMP_TYPE.GROUND );
        if ( type1.equals( COMP_TYPE.MYPLAYER ) && contact2Ground ) {
            final RemoveByZoneComponent zoneComponent = MyMapper.REMOVE_BY_ZONE.get( entity02 );

            int lastID = DynamicLevels.getCurrent();
            int newID = zoneComponent.id;
            DynamicLevels.setCurrent( newID );
            if ( !MyMapper.GROUNDED.get( entity01 ).grounded && lastID != newID ) {
                DynamicLevels.reload();
            }
            MyMapper.GROUNDED.get( entity01 ).grounded = true;
        }

        final boolean contact2Ladder = type2.equals( COMP_TYPE.LADDER );
        if ( type1.equals( COMP_TYPE.MYPLAYER ) && contact2Ladder ) {
            MyPlayer.onLadder = true;
        }

        final boolean contact2Collectable = type2.equals( COMP_TYPE.COLLECTABLE );
        final boolean outOfCamera = MyMapper.OUT_OF_CAMERA.has( entity02 );
        if ( !outOfCamera ) {
            if ( type1.equals( COMP_TYPE.MYPLAYER ) && contact2Collectable ) {
                MyPlayer.addToBag( MyMapper.COLLECTABLE.get( entity02 ).item );
                entity02.add( new OutOfCameraComponent() );
                entity02.add( new RemoveByTimeComponent( 0 ) );

                // Устанавливаем в levelObject флаг, чтобы предмет
                // не создался снова, при перезагрузке уровня
                if ( MyMapper.LEVEL_OBJECT.has( entity02 ) ) {
                    MyMapper.LEVEL_OBJECT.get( entity02 ).levelObject.useEntity();
                }


                // show bubbles
                if(MyMapper.PHYSICAL.has( entity02 )) {
                    Vector3 bubblePosition = new Vector3(  );
                    MyMapper.PHYSICAL.get( entity02 ).body.getWorldTransform().getTranslation( bubblePosition );
                    for(int i=0;i<5;i++) {
                        float bubbleTime = MathUtils.random( 0.05f, 0.25f );
                        /*AshleyWorld.getPooledEngine().addEntity(
                                EntitySubs.LightBubblesEffect( bubblePosition, bubbleTime * 6 ) );*/

                        Entity entity = AshleyWorld.getPooledEngine().createEntity();
                        entity.add( DecalSubs.BubbleEffect( bubbleTime ) );
                        entity.add( new PositionComponent( bubblePosition ) );
                        entity.add( new RemoveByTimeComponent( bubbleTime ) );
                        AshleyWorld.getPooledEngine().addEntity( entity );
                    }
                }
            }
        }
    }


    private void endContactEntities ( Entity entity01, Entity entity02 ) {

        final COMP_TYPE type1 = MyMapper.TYPE.get( entity01 ).type;
        final COMP_TYPE type2 = MyMapper.TYPE.get( entity02 ).type;

        if ( DEBUG_CONTACTS ) {
            stringBuilder.setLength( 0 );
            stringBuilder.append( type1 );
            stringBuilder.append( " -> " );
            stringBuilder.append( type2 );
            Gdx.app.debug( "endContact", stringBuilder.toString() );
        }

        final boolean contact2Ground = type2.equals( COMP_TYPE.GROUND );
        if ( type1.equals( COMP_TYPE.MYPLAYER ) && contact2Ground ) {
            MyMapper.GROUNDED.get( entity01 ).grounded = false;
            DynamicLevels.reload();
            //return;
        }

        final boolean contact2Ladder = type2.equals( COMP_TYPE.LADDER );
        if ( type1.equals( COMP_TYPE.MYPLAYER ) && contact2Ladder ) {
            MyPlayer.onLadder = false;
        }
    }


    public void setEntities ( ImmutableArray< Entity > entities ) {
        this.entities = entities;
    }


    @Override
    public void dispose () {
        super.dispose();

        entity1 = null;
        entity2 = null;
    }
}