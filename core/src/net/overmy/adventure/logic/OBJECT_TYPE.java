package net.overmy.adventure.logic;

/*
        Created by Andrey Mikheev on 28.02.2018
        Contact me → http://vk.com/id17317
*/

public enum OBJECT_TYPE {
    LADDER,
    PICKABLE,
    HOVER_COLLECTABLE,
    COLLECTABLE,
    NPC,
    ENEMY
}