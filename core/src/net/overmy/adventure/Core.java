package net.overmy.adventure;

/*
     Created by Andrey Mikheev on 29.09.2017
     Contact me → http://vk.com/id17317
 */

public final class Core {

    public static final float FADE      = 0.3f;
    public static final float FADE_HALF = FADE * 0.5f;
    public static int WIDTH;
    public static int WIDTH_HALF;
    public static int HEIGHT;
    public static int LEFT_BOUND;
    public static int RIGHT_BOUND;
    public static int BOTTOM_BOUND;
    public static int TOP_BOUND;
    public static int HEIGHT_HALF;



    private Core() {
    }



    static void resize( int width, int height ) {
        WIDTH = width;
        HEIGHT = height;
        WIDTH_HALF = WIDTH / 2;
        HEIGHT_HALF = HEIGHT / 2;
        RIGHT_BOUND = (int) (WIDTH * 0.95f);
        LEFT_BOUND = (int) (WIDTH * 0.05f);
        TOP_BOUND = (int) (HEIGHT * 0.95f);
        BOTTOM_BOUND = (int) (HEIGHT * 0.15f);
    }
}
