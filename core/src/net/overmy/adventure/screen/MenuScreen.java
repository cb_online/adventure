package net.overmy.adventure.screen;

/*
      Created by Andrey Mikheev on 10.10.2017
      Contact me → http://vk.com/id17317
 */

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import net.overmy.adventure.Core;
import net.overmy.adventure.MyGdxGame;
import net.overmy.adventure.MyRender;
import net.overmy.adventure.resources.FontAsset;
import net.overmy.adventure.resources.GameColor;
import net.overmy.adventure.resources.Settings;
import net.overmy.adventure.utils.GFXHelper;
import net.overmy.adventure.utils.UIHelper;

public class MenuScreen extends Base2DScreen {

    private static Group  introGroup        = null;
    private static Group  settingsGroup     = null;
    private static Slider soundBar          = null;
    private static Slider musicBar          = null;
    private static Slider verticalSensBar   = null;
    private static Slider horizontalSensBar = null;

    private GUI_TYPE guiType;


    public MenuScreen ( MyGdxGame game ) {
        super( game );
    }


    @Override
    public void show () {
        super.show();

        MyRender.getStage().addActor( settingsGroup = new Group() );
        MyRender.getStage().addActor( introGroup = new Group() );

        showIntroGUI();
    }


    @Override
    public void dispose () {
        super.dispose();
        introGroup = null;
        settingsGroup = null;
        soundBar = null;
        musicBar = null;
        verticalSensBar = null;
        horizontalSensBar = null;
    }


    @Override
    public void backButton () {
        if ( guiType == GUI_TYPE.SETTINGS ) {
            showIntroGUI();
            //settingsGroup.addAction( Actions.alpha( 0, Core.FADE ) );
            UIHelper.rollOut( settingsGroup,
                              0, 0,
                              -Core.WIDTH, 0
                            );

            Settings.HORIZ_SENS.setInteger( (int) horizontalSensBar.getValue() );
            Settings.VERT_SENS.setInteger( (int) verticalSensBar.getValue() );
            Settings.MUSIC.setInteger( (int) musicBar.getValue() );
            Settings.SOUND.setInteger( (int) soundBar.getValue() );
        } else {
            transitionTo( MyGdxGame.SCREEN_TYPE.EXIT );

            UIHelper.rollOut( introGroup, 0, 0, -Core.WIDTH_HALF, 0 );
        }
    }


    private void showIntroGUI () {
        guiType = GUI_TYPE.MAIN_MENU;
        introGroup.clear(); // disable clearGroup Runnable

        final Label startLabel = new Label( "start", FontAsset.MONEY.getStyle() );
        final Label settingsLabel = new Label( "settings", FontAsset.MONEY.getStyle() );

        introGroup.addActor( startLabel );
        introGroup.addActor( settingsLabel );

        UIHelper.rollIn( startLabel,
                         Core.WIDTH, Core.HEIGHT * 0.4f,
                         Core.WIDTH * 0.6f, Core.HEIGHT * 0.4f
                       );

        startLabel.addListener( new ClickListener() {
            @Override
            public void clicked ( InputEvent event, float x, float y ) {
                Gdx.app.debug( "", "click startLabel" );
                UIHelper.clickAnimation( startLabel );
                transitionTo( MyGdxGame.SCREEN_TYPE.LOADING_GAME );

                UIHelper.rollOut( startLabel,
                                  Core.WIDTH * 0.6f, Core.HEIGHT * 0.4f,
                                  -Core.WIDTH_HALF, Core.HEIGHT * 0.4f
                                );
                UIHelper.rollOut( settingsLabel,
                                  Core.WIDTH * 0.6f, Core.HEIGHT * 0.25f,
                                  -Core.WIDTH_HALF, Core.HEIGHT * 0.25f
                                );
            }
        } );

        UIHelper.rollIn( settingsLabel,
                         Core.WIDTH, Core.HEIGHT * 0.25f,
                         Core.WIDTH * 0.6f, Core.HEIGHT * 0.25f
                       );
        settingsLabel.addListener( new ClickListener() {
            @Override
            public void clicked ( InputEvent event, float x, float y ) {
                Gdx.app.debug( "", "click settingsLabel" );
                showSettingsGUI();
                UIHelper.rollOut( settingsLabel,
                                  Core.WIDTH * 0.6f, Core.HEIGHT * 0.25f,
                                  -Core.WIDTH_HALF, Core.HEIGHT * 0.25f
                                );
                UIHelper.rollOut( startLabel,
                                  Core.WIDTH * 0.6f, Core.HEIGHT * 0.4f,
                                  -Core.WIDTH_HALF, Core.HEIGHT * 0.4f
                                );
            }
        } );
    }


    private void showSettingsGUI () {
        guiType = GUI_TYPE.SETTINGS;
        settingsGroup.clear(); // clear Runnable
        settingsGroup.setPosition( 0,0 );

        Image bgImage = new Image( GFXHelper.createSpriteRGB888( Core.WIDTH * 0.9f,
                                                                 Core.HEIGHT * 0.8f ) );
        bgImage.setPosition( Core.WIDTH * 0.05f, Core.HEIGHT * 0.1f );
        bgImage.setColor( GameColor.BLACKGL.get() );
        settingsGroup.addActor( bgImage );

        settingsGroup.addAction( Actions.alpha( 1, Core.FADE ) );

        final Label settingsLabel = new Label( "настройки", FontAsset.LOCATION_TITLE.getStyle() );
        settingsLabel.setPosition( Core.WIDTH / 2 - settingsLabel.getWidth() / 2,
                                   Core.HEIGHT * 0.75f );
        settingsGroup.addActor( settingsLabel );

        final Label soundLabel = new Label( "звуки", FontAsset.LOCATION_TITLE.getStyle() );
        soundLabel.setPosition( Core.HEIGHT * 0.15f, Core.HEIGHT * 0.6f );
        settingsGroup.addActor( soundLabel );

        Texture myTexture = GFXHelper.createTexture( 4, 4 );
        Drawable myDrawable = new TextureRegionDrawable( new TextureRegion( myTexture ) );

        Texture myTexture2 = GFXHelper.createTexture( 4, 4 );
        Drawable myDrawable2 = new TextureRegionDrawable( new TextureRegion( myTexture2 ) );

        if ( soundBar == null ) {
            final Slider.SliderStyle sliderStyle = new Slider.SliderStyle();

            sliderStyle.knob = myDrawable;
            sliderStyle.knob.setMinWidth( Core.HEIGHT * 0.14f );
            sliderStyle.knob.setMinHeight( Core.HEIGHT * 0.1f );

            sliderStyle.background = myDrawable2;
            sliderStyle.background.setMinWidth( Core.WIDTH * 0.55f );
            sliderStyle.background.setMinHeight( Core.HEIGHT * 0.05f );

            soundBar = new Slider( 0, 100, 1, false, sliderStyle );
            soundBar.setWidth( Core.WIDTH * 0.55f );
            soundBar.invalidate();
            soundBar.setValue( Settings.SOUND.getInteger() );
            soundBar.setPosition( Core.WIDTH * 0.35f, Core.HEIGHT * 0.6f );
        }

        settingsGroup.addActor( soundBar );

        final Label musicLabel = new Label( "музыка", FontAsset.LOCATION_TITLE.getStyle() );
        musicLabel.setPosition( Core.HEIGHT * 0.15f, Core.HEIGHT * 0.45f );
        settingsGroup.addActor( musicLabel );

        if ( musicBar == null ) {
            final Slider.SliderStyle sliderStyle = new Slider.SliderStyle();

            sliderStyle.knob = myDrawable;
            sliderStyle.knob.setMinWidth( Core.HEIGHT * 0.14f );
            sliderStyle.knob.setMinHeight( Core.HEIGHT * 0.1f );

            sliderStyle.background = myDrawable2;
            sliderStyle.background.setMinWidth( Core.WIDTH * 0.55f );
            sliderStyle.background.setMinHeight( Core.HEIGHT * 0.05f );

            musicBar = new Slider( 0, 100, 1, false, sliderStyle );
            musicBar.setWidth( Core.WIDTH * 0.55f );
            musicBar.invalidate();
            musicBar.setValue( Settings.MUSIC.getInteger() );
            musicBar.setPosition( Core.WIDTH * 0.35f, Core.HEIGHT * 0.45f );
        }

        settingsGroup.addActor( musicBar );

        final Label horizontal = new Label( "горизонтально", FontAsset.LOCATION_TITLE.getStyle() );
        horizontal.setPosition( Core.HEIGHT * 0.15f, Core.HEIGHT * 0.3f );
        settingsGroup.addActor( horizontal );

        if ( horizontalSensBar == null ) {
            final Slider.SliderStyle sliderStyle = new Slider.SliderStyle();

            sliderStyle.knob = myDrawable;
            sliderStyle.knob.setMinWidth( Core.HEIGHT * 0.14f );
            sliderStyle.knob.setMinHeight( Core.HEIGHT * 0.1f );

            sliderStyle.background = myDrawable2;
            sliderStyle.background.setMinWidth( Core.WIDTH * 0.55f );
            sliderStyle.background.setMinHeight( Core.HEIGHT * 0.05f );

            horizontalSensBar = new Slider( 0, 100, 5, false, sliderStyle );
            horizontalSensBar.setWidth( Core.WIDTH * 0.55f );
            horizontalSensBar.invalidate();
            horizontalSensBar.setValue( Settings.HORIZ_SENS.getInteger() );
            horizontalSensBar.setPosition( Core.WIDTH * 0.35f, Core.HEIGHT * 0.3f );
        }

        settingsGroup.addActor( horizontalSensBar );

        final Label vertical = new Label( "вертикально", FontAsset.LOCATION_TITLE.getStyle() );
        vertical.setPosition( Core.HEIGHT * 0.15f, Core.HEIGHT * 0.15f );
        settingsGroup.addActor( vertical );

        if ( verticalSensBar == null ) {
            final Slider.SliderStyle sliderStyle = new Slider.SliderStyle();

            sliderStyle.knob = myDrawable;
            sliderStyle.knob.setMinWidth( Core.HEIGHT * 0.14f );
            sliderStyle.knob.setMinHeight( Core.HEIGHT * 0.1f );

            sliderStyle.background = myDrawable2;
            sliderStyle.background.setMinWidth( Core.WIDTH * 0.55f );
            sliderStyle.background.setMinHeight( Core.HEIGHT * 0.05f );

            verticalSensBar = new Slider( 0, 100, 5, false, sliderStyle );
            verticalSensBar.setWidth( Core.WIDTH * 0.55f );
            verticalSensBar.invalidate();
            verticalSensBar.setValue( Settings.VERT_SENS.getInteger() );
            verticalSensBar.setPosition( Core.WIDTH * 0.35f, Core.HEIGHT * 0.15f );
        }

        settingsGroup.addActor( verticalSensBar );

        /// sound, music, aim, vertical velocity, horizontal velocity

        //sdfsdf

    }


    private enum GUI_TYPE {
        MAIN_MENU,
        SETTINGS
    }
}
